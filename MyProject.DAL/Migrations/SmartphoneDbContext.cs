using Microsoft.EntityFrameworkCore;

namespace MyProject
{
    public class SmartphoneDbContext : DbContext
    {

        public SmartphoneDbContext(DbContextOptions options) : base(options) 
        {

        }
        public DbSet<Smartphone> Smartphones { get; set; } 

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Smartphone>(x =>
            {
                x.ToTable("Smartphones");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    }
} 