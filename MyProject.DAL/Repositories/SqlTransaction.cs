﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MyProject.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyProject.DAL.Repositories
{
    public class SqlTransaction : ISqlTransaction
    {
        private readonly IDbContextTransaction _transaction;

        private SqlTransaction(SmartphoneDbContext context, System.Data.IsolationLevel level)
        {
            _transaction = context.Database.BeginTransaction(level);
        }
        public static ISqlTransaction Begin(SmartphoneDbContext context, System.Data.IsolationLevel level)
        {
            return new SqlTransaction(context, level);
        }
        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback(); 
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _transaction.Dispose();
            }
        } 

    }
}
