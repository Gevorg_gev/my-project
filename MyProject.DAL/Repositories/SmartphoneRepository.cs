﻿using MyProject.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyProject.DAL.Repositories
{
    public class SmartphoneRepository : SqlRepositoryBase<Smartphone>, ISmartphoneRepository
    {
        public SmartphoneRepository(SmartphoneDbContext smartphoneDbContext)
            : base(smartphoneDbContext) 
        {
            
        }
    }
}
