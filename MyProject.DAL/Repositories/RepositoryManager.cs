﻿using MyProject.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.DAL.Repositories
{
    public class RepositoryManager : IRepositoryManager 
    {
        private readonly SmartphoneDbContext smartphoneDbContext;
        public RepositoryManager(SmartphoneDbContext smartphoneDbContext)
        {
            this.smartphoneDbContext = smartphoneDbContext; 
        }
        private ISmartphoneRepository _smartphones;
        public ISmartphoneRepository Smartphones => _smartphones ?? (_smartphones = new SmartphoneRepository(smartphoneDbContext));

        public IUserRepository Users => throw new NotImplementedException();

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(smartphoneDbContext, isolation); 
        }

        public int SaveChanges()
        {
            return smartphoneDbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return smartphoneDbContext.SaveChangesAsync(); 
        }
    }
}
