using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 


namespace MyProject
{
    [ApiController]
    [Route("[controller]")] 
    public class SmartphonesController : ControllerBase 
    {
        private readonly SmartphoneDbContext smartphoneDbContext; 
        public SmartphonesController(SmartphoneDbContext smartphoneDbContext) 
        {
            this.smartphoneDbContext = smartphoneDbContext;
        }

        private readonly string[] Smartphone_Brand = new[]
        {
            "Samsung" , "Apple" , "Xiaomi" , "Huawei" , "Alcatel" 
        };

        private readonly string[] Smartphone_Model = new[] 
        {
            "Samsung Galaxy S21 Ultra" , "IPhone 12 Pro Max" , "Xiaomi Mi Note 10 Lite" ,
            "Huawei Honor X5" , "Alcatel OneTouch Ocean" 
        };

        private readonly string[] Smartphone_Color = new[]
        {
            "Matt Black" , "Midnight Green" , "Ceramic White" , "Space Grey" , "Rose Gold" 
        };

        private readonly string[] Smartphone_Country = new[]
        {
            "South Korea" , "USA" , "China" , "Thailand" 
        };

        private readonly string[] Smartphone_ScreenResolution = new[]
        {
            "HD 720p" , "FHD 1080p" , "QHD 1440p" , "UHD 2160p" 
        };

        private readonly string[] Smartphone_CPU = new[] 
        {
            "Samsung Exynos 9825" , "Apple A13 Bionic Chip" , "Qualcomm Snapdragon 630" 
        };


        [HttpGet]
        public IEnumerable GetSmartphones()
        {
            var rng = new Random();
            return Enumerable.Range(1, 9).Select(index => new Smartphone
            {
                Id = rng.Next(1,9),
                Brand = Smartphone_Brand[rng.Next(Smartphone_Brand.Length)],
                Model = Smartphone_Model[rng.Next(Smartphone_Model.Length)],
                Color = Smartphone_Color[rng.Next(Smartphone_Color.Length)],
                Country = Smartphone_Country[rng.Next(Smartphone_Country.Length)],
                DisplaySize = rng.Next(4,6), 
                ScreenResolution = Smartphone_ScreenResolution[rng.Next(Smartphone_ScreenResolution.Length)],
                CPU = Smartphone_CPU[rng.Next(Smartphone_CPU.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddSmartphones([FromBody] Smartphone smartphone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            smartphoneDbContext.Add(smartphone);


            return Ok(); 
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveSmartphones([FromRoute] int id)
        {
            smartphoneDbContext.Remove(id); 


            return Ok();  
        }

        [HttpPut("{id}")]
        public IActionResult UpdateSmartphones([FromBody] Smartphone smartphone, [FromRoute] int id)
        {
            smartphoneDbContext.Update(id); 


            return Ok();
        }
    }
}