﻿using MyProject.Core.Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MyProject.BLL.Operations
{
    public class SmartphoneBL : ISmartphoneBL
    {
        private readonly SmartphoneDbContext smartphoneDbContext;

        public Smartphone AddSmartphones(Smartphone smartphone)
        {
            smartphoneDbContext.Smartphones.Add(smartphone);
            smartphoneDbContext.SaveChanges();


            return smartphone;
        }

        public IEnumerable GetSmartphones()
        {
            throw new NotImplementedException();
        }

        public bool RemoveSmartphones(int id) 
        {
            var rmv = smartphoneDbContext.Smartphones.Find(id); 
            if (rmv == null)
            {
                return true;
            }

            smartphoneDbContext.Smartphones.Remove(rmv);
            smartphoneDbContext.SaveChanges();


            return false;
        }

        public bool UpdateSmartphones(Smartphone smartphone, int id)
        {
            var smartphoneToUpdate = smartphoneDbContext.Smartphones.Find(id);
            if (smartphoneToUpdate == null)
            {
                return true; 
            }


            smartphoneToUpdate.Id = smartphone.Id;
            smartphoneToUpdate.Brand = smartphone.Brand;
            smartphoneToUpdate.Model = smartphone.Model;
            smartphoneToUpdate.Color = smartphone.Color;
            smartphoneToUpdate.Country = smartphone.Country;
            smartphoneToUpdate.DisplaySize = smartphone.DisplaySize;
            smartphoneToUpdate.ScreenResolution = smartphone.ScreenResolution;
            smartphoneToUpdate.CPU = smartphone.CPU;

            smartphoneDbContext.Smartphones.Update(smartphoneToUpdate);
            smartphoneDbContext.SaveChanges();


            return false; 
        }
    }
}
