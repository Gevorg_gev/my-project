namespace MyProject
{
    public class Smartphone
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Country { get; set; }
        public int DisplaySize  { get; set; }
        public string ScreenResolution { get; set; } 
        public string CPU { get; set; } 
    }
}