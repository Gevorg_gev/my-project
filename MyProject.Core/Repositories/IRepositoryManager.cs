﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.Core.Repositories
{
    public interface IRepositoryManager
    {
        ISmartphoneRepository Smartphones { get; }
        int SaveChanges();
        IUserRepository Users { get; } 
        Task<int> SaveChangesAsync(); 
        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
    }
}
