﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyProject.Core.Repositories
{
    public interface ISqlTransaction
    {
        void Commit();
        void Rollback(); 
    }
}
