﻿using MyProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyProject.Core.Repositories
{
    public interface IUserRepository : ISqlRepository<User> 
    {
    }
}
