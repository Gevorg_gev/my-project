﻿using Microsoft.AspNetCore.Http;
using MyProject.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.Core.Operations
{
    public interface IUserBL
    {
        Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext);
        Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext);
        Task LogOutAsync(HttpContext httpContext); 
    }
}
