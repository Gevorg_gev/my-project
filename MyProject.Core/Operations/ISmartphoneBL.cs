﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MyProject.Core.Operations
{
    public interface ISmartphoneBL
    {
        IEnumerable GetSmartphones();
        bool RemoveSmartphones(int id); 
        bool UpdateSmartphones(Smartphone smartphone, int id);
        Smartphone AddSmartphones(Smartphone smartphone); 
    }
}
